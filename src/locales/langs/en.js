export default {
  message: {
    sec1: {
      title: 'What’s an UNZIP EVENT?',
      description: 'UNZIP is an event which is open for those who interested in sharing their ideas, knowledges or experiences to propose their topics. The proposed topic does not have to be serious or even related to computer or technology things. You can come either as a group or individual. If you want to share something, just come.'
    },
    sec2: {
      title: 'Objective',
      description: 'We want UNZIP to become an idea-sharing space, which everyone can meet and exchange their ideas. They can find and talk to those with similar interests. We also want to stimulate all attendee’s curiosity and their enthusiasm to learn new things.'
    },
    sec3: {
      title: 'Want to become a speaker in UNZIP?',
      description: 'We are accepting proposals for the topic until Feb 26, 2019. The proposed topic must not be contrary to the law or in high risk to become so. The speaker must be able to cover the topic in 25 minutes speaking time. The speaker can speak in either Thai or English.'
    },
    sec4: {
      title: 'Who can attend this event?',
      description: 'All KMUTT students, staffs and everyone who are interested in this event.'
    },
    sec5: {
      title: 'Time Table'
    },
    sec6: {
      title: 'Location'
    },
    navbar:{
      regis: 'Register',
      col1: 'Time Table',
      col2: 'Location',
      col3: 'Sponsors',
      col4: 'FAQ',
      col5: 'Contact Us'
    },
    maintxt: {
      date: 'March 16, 2019',
      location: 'King Mongkut\'s University of Technology Thonburi'
    },
    btn: {
      regis: 'Register'
    }
  }
}
